﻿using System.Collections.Generic;
using Blog.DAL.Infrastructure;
using Blog.DAL.Model;
using System;

namespace Blog.DAL.Repository
{
    public class BlogRepository
    {
        private readonly BlogContext _context;

        public BlogRepository()
        {
            _context = new BlogContext();
        }

        public IEnumerable<Post> GetAllPosts()
        {
            return _context.Posts;
        }

        public void AddPost(string author, string content)
        {
            if (String.IsNullOrEmpty(author) || String.IsNullOrEmpty(content))
                throw new Exception("Data is not valid");

            _context.Posts.Add(new Post {Author=author, Content = content});
            _context.SaveChanges();
        }
    }
}
