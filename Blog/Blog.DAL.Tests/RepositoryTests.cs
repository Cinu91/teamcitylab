﻿using System;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using Blog.DAL.Infrastructure;
using Blog.DAL.Model;
using Blog.DAL.Repository;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics;
using TDD.DbTestHelpers.Core;

namespace Blog.DAL.Tests
{
    [TestClass]
    public class RepositoryTests : DbBaseTest<BlogFixtures>
    {
        [TestMethod]
        public void GetAllPost_TwoPostsInDb_ReturnTwoPosts()
        {
            // arrange
            var context = new BlogContext();
            context.Database.CreateIfNotExists();
            var repository = new BlogRepository();

            // act
            var result = repository.GetAllPosts();
            // assert
            Assert.AreEqual(2, result.Count());
        }

        [TestMethod]
        public void AddPost_AddOnePostToDb_ReturnOnePost()
        {
            // arrange
            var context = new BlogContext();
            context.Posts.ToList().Clear();
            var repository = new BlogRepository();
            
            // act
            repository.AddPost("next", "blablabla");
            var result = repository.GetAllPosts();
            
            // assert
            Assert.AreEqual(3, result.Count());
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void AddPost_AddInvalidPostToDb_ShouldThrowException()
        {
            // arrange
            var repository = new BlogRepository();

            // act
            repository.AddPost("", null);
        }
    }
}
